#pragma checksum "D:\projects\ecomap\Views\Home\Account.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "1451a19d5957a29fdb2f55d1ac9046770860c742"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Account), @"mvc.1.0.view", @"/Views/Home/Account.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\projects\ecomap\Views\_ViewImports.cshtml"
using WebApplication1;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\projects\ecomap\Views\_ViewImports.cshtml"
using WebApplication1.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"1451a19d5957a29fdb2f55d1ac9046770860c742", @"/Views/Home/Account.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"729efaa87342638aecfe1a972ce9f9f8dff55b4c", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Account : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<User>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "D:\projects\ecomap\Views\Home\Account.cshtml"
  
    ViewData["Title"] = "Account";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<link rel=""stylesheet"" href=""/mdl/material.min.css"">
<script src=""./mdl/material.min.js""></script>

<style>
    .demo-card-square.mdl-card {
        width: 320px;
        height: 320px;
    }

    .demo-card-square > .mdl-card__title {
        color: #fff;
        background: bottom right 15% no-repeat #46B6AC;
    }
</style>

<div class=""demo-card-square mdl-card mdl-shadow--2dp"">
    <div class=""mdl-card__title mdl-card--expand"">
        <h2 class=""mdl-card__title-text"">
            <div class=""u-photo"" /> <img alt=""profile photo"" border=""50"" />
            <div class=""u-nick"" />");
#nullable restore
#line 25 "D:\projects\ecomap\Views\Home\Account.cshtml"
                             Write(Model.NickName);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            <div class=\"u-rating\" />");
#nullable restore
#line 26 "D:\projects\ecomap\Views\Home\Account.cshtml"
                               Write(Model.Rating);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n        </h2>\r\n    </div>\r\n    <div class=\"mdl-card__supporting-text\">\r\n        <div class=\"u-markers-list\">\r\n");
#nullable restore
#line 32 "D:\projects\ecomap\Views\Home\Account.cshtml"
              
                foreach (Marker m in Model.markers)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <div> ");
#nullable restore
#line 35 "D:\projects\ecomap\Views\Home\Account.cshtml"
                     Write(m.Coords);

#line default
#line hidden
#nullable disable
            WriteLiteral(" ");
#nullable restore
#line 35 "D:\projects\ecomap\Views\Home\Account.cshtml"
                               Write(m.Description);

#line default
#line hidden
#nullable disable
            WriteLiteral(" </div>\r\n");
#nullable restore
#line 36 "D:\projects\ecomap\Views\Home\Account.cshtml"
                    foreach (User u in m.MarkerUsers.Select(mu => mu.user))
                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <div>");
#nullable restore
#line 38 "D:\projects\ecomap\Views\Home\Account.cshtml"
                        Write(u.NickName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</div>\r\n");
#nullable restore
#line 39 "D:\projects\ecomap\Views\Home\Account.cshtml"
                    }
                }
            

#line default
#line hidden
#nullable disable
            WriteLiteral("        </div>\r\n    </div>\r\n    <div class=\"mdl-card__actions mdl-card--border\" align=\"center\">\r\n        <a class=\"mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect\">\r\n            logout\r\n        </a>\r\n    </div>\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<User> Html { get; private set; }
    }
}
#pragma warning restore 1591
