﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

public class ApplicationManager
{
    #region Singleton

    private static ApplicationManager instance;
    public static ApplicationManager GetInstance()
    {
        if (instance == null) instance = new ApplicationManager();
        return instance;
    }

    #endregion

    public string connString = "";
    public EcoContext db = new EcoContext();

}
