﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class User
    {
        public int Id { get; set; }
        public string NickName { get; set; }
        public int Rating { get; set; }

        public virtual ICollection<Marker> markers { get; set; }

        public virtual List<MarkerUser> MarkerUsers { get; set; }
    }
}
